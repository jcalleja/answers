var shell = require('shelljs');

var LOCAL_M2 = '/Users/justin/.m2/repository';
var DIR_OF_DOJO_JAR = LOCAL_M2 + '/com/ricston/bunya/dojo/1.7.2';
var DOJO_JAR = 'dojo-1.7.2.jar';

exports.testLocalM2HomeHasDojo = function(test) {
	var dojojsLocation = 'META-INF/resources/dojo/dojo.js'

	shell.cd(DIR_OF_DOJO_JAR);
	var toc = shell.exec('jar tf ' + DOJO_JAR, { silent: true });
	var tocArray = toc.output.split('\n');
	test.ok(contains(tocArray, dojojsLocation), DIR_OF_DOJO_JAR + '/' + DOJO_JAR + ' does not have dojo.js in the expected location');

	test.done();
};


function contains(a, obj) {
    var i = a.length;
    while (i--) {
       if (a[i] === obj) {
           return true;
       }
    }
    return false;
}
